package com.coding.challenge.service;

import com.coding.challenge.dto.ProductDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class DefaultProductServiceTest {

    @Autowired
    ProductService productService;

    @Test
    public void getProductById() throws Exception {

        ProductDTO productDTO = productService.getProductById(2);
        assertThat(productDTO.getId()).isEqualTo(2L);
    }

    @Test void getAllProducts() throws Exception {
        Set<ProductDTO> productDTOSet = productService.getAllProducts();
        assertThat(productDTOSet.size()).isEqualTo(3);
    }
}
