package com.coding.challenge.service;

import com.coding.challenge.dto.CartDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class DefaultCartServiceTest {

    @Autowired
    CartService cartService;

    @Test
    public void getCartById() throws Exception {

        CartDTO cartDTO = cartService.getCartById(6);
        assertThat(cartDTO.getId()).isEqualTo(6);
    }

    @Test
    public void updateCart() throws Exception {

        CartDTO cartDTO = cartService.updateCart(6, 3, 2);
        assertThat(cartDTO.getCartEntries()
                .stream()
                .filter(
                        cartEntryDTO ->
                                cartEntryDTO.getProduct().getId().equals(3L))
                .findFirst().get().getProduct().getId())
                .isEqualTo(3L);

        cartDTO = cartService.getCartById(6);

        assertThat(cartDTO.getCartEntries()
                .stream()
                .filter(
                        cartEntryDTO ->
                                cartEntryDTO.getProduct().getId().equals(3L))
                .findFirst().get().getProduct().getId())
                .isEqualTo(3L);
    }
}
