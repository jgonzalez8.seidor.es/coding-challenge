package com.coding.challenge.service;

import com.coding.challenge.dto.CartEntryDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class DefaultCartEntryServiceTest {

    @Autowired
    CartEntryService cartEntryService;

    @Test
    public void getCartEntryById() throws Exception {

        CartEntryDTO cartEntryDTO = cartEntryService.getCartEntryById(4);
        assertThat(cartEntryDTO.getId()).isEqualTo(4);
    }

    @Test
    public void createCartEntry() throws Exception {

        Long res = cartEntryService.createCartEntry(1, 3);
        assertThat(res).isGreaterThan(0);
    }
}
