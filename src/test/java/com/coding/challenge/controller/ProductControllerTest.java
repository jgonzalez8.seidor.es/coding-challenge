package com.coding.challenge.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {


    public static final String PRODUCT_NAME1 = "PR001";
    public static final String PRODUCT_NAME2 = "PR002";
    public static final String PRODUCT_NAME3 = "PR003";
    @Autowired
    private MockMvc mvc;

    @Test
    public void getProductById() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/v1/product/2").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(PRODUCT_NAME2)));
    }

    @Test
    public void getAllProducts() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/v1/product/all").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(PRODUCT_NAME1)))
                .andExpect(content().string(containsString(PRODUCT_NAME2)))
                .andExpect(content().string(containsString(PRODUCT_NAME3)));
    }
}
