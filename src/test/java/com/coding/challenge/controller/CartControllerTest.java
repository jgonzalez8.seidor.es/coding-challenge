package com.coding.challenge.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CartControllerTest {

    public static final String NEW_PRODUCT_DESCRIPTION = "DESCRIPTION_003";
    public static final String CART_ID = "6";
    @Autowired
    private MockMvc mvc;

    @Test
    public void getCartById() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/v1/cart/6").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(CART_ID)));
    }

    @Test
    public void updateCart() throws Exception {
        mvc.perform(MockMvcRequestBuilders.put("/v1/cart/updateCart?cartId=6&productId=3&quantity=8").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(NEW_PRODUCT_DESCRIPTION)));
    }
}
