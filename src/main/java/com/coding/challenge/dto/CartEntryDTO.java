package com.coding.challenge.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class CartEntryDTO {

    private Long id;
    private ProductDTO product;
    private long quantity;

    public CartEntryDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(final ProductDTO product) {
        this.product = product;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(final long quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("price")
    public BigDecimal getPrice() {

        return product.getPrice().multiply(BigDecimal.valueOf(this.quantity));
    }

    @Override
    public String toString() {
        return "{\"_class\":\"CartEntryDTO\", " +
                "\"id\":" + (id == null ? "null" : "\"" + id + "\"") + ", " +
                "\"productId\":" + (product == null ? "null" : product.getId()) + ", " +
                "\"quantity\":\"" + quantity + "\"" +
                "}";
    }
}
