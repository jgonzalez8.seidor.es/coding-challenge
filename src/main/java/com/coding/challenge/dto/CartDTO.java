package com.coding.challenge.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Set;

public class CartDTO {

    private Long id;
    private Set<CartEntryDTO> cartEntries;

    public CartDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Set<CartEntryDTO> getCartEntries() {
        return cartEntries;
    }

    public void setCartEntries(final Set<CartEntryDTO> cartEntries) {
        this.cartEntries = cartEntries;
    }

    @JsonProperty("totalPrice")
    public BigDecimal getTotalPrice() {

        return getCartEntries().stream().map(CartEntryDTO::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public String toString() {
        return "{\"_class\":\"CartDTO\", " +
                "\"id\":" + (id == null ? "null" : "\"" + id + "\"") +
                "}";
    }
}
