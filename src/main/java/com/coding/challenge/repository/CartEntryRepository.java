package com.coding.challenge.repository;

import com.coding.challenge.entity.CartEntry;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartEntryRepository extends CrudRepository<CartEntry, Long> {

    Optional<CartEntry> findById(Long id);
}
