package com.coding.challenge;

import com.coding.challenge.entity.Cart;
import com.coding.challenge.entity.CartEntry;
import com.coding.challenge.entity.Product;
import com.coding.challenge.repository.CartEntryRepository;
import com.coding.challenge.repository.CartRepository;
import com.coding.challenge.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootApplication
public class ChallengeApplication {

    private static final Logger log = LoggerFactory.getLogger(ChallengeApplication.class);


    public static void main(String[] args) {
        SpringApplication.run(ChallengeApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(final ProductRepository productRepository,
                                  final CartEntryRepository cartEntryRepository,
                                  final CartRepository cartRepository) {
        return args -> {

            products(productRepository);

            cartEntries(productRepository, cartEntryRepository);

            cart(cartEntryRepository, cartRepository);
        };
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    private void cart(final CartEntryRepository cartEntryRepository,
                      final CartRepository cartRepository) {

        log.info("===== CREATING CART =====");
        Long cartId = createCart(cartEntryRepository, cartRepository);
        log.info("---------------------------");
        log.info("");

        log.info("===== GETTING CART =====");
        getCartById(cartRepository, cartId);
        log.info("---------------------------");
        log.info("");
    }

    private void getCartById(final CartRepository cartRepository, final Long cartId) {

        log.info(cartRepository.findById(cartId).toString());
    }

    private Long createCart(final CartEntryRepository cartEntryRepository,
                            final CartRepository cartRepository) {

        final Set<CartEntry> cartEntries = new HashSet<>();
        cartEntryRepository.findAll().forEach(cartEntries::add);

        final Cart cart = new Cart(cartEntries);
        cartRepository.save(cart);

        return cart.getId();
    }

    private void cartEntries(final ProductRepository productRepository,
                             final CartEntryRepository cartEntryRepository) {

        log.info("===== CREATING CARTENTRIES =====");
        final List<Long> cartEntriesIds = createCartEntries(productRepository, cartEntryRepository);
        log.info("---------------------------");
        log.info("");

        log.info("===== GETTING ALL CARTENTRIES =====");
        getAllCartEntries(cartEntryRepository);
        log.info("---------------------------");
        log.info("");

        log.info("===== GETTING CARTENTRY BY ID =====");
        getCartEntryById(cartEntryRepository, cartEntriesIds.get(0));
        log.info("---------------------------");
        log.info("");
    }

    private void getCartEntryById(final CartEntryRepository cartEntryRepository, final Long id) {

        log.info(cartEntryRepository.findById(id).toString());
    }

    private void getAllCartEntries(final CartEntryRepository cartEntryRepository) {

        final Iterable<CartEntry> cartEntries = cartEntryRepository.findAll();

        for (CartEntry cartEntry : cartEntries) {
            log.info(cartEntry.toString());
        }
    }

    private List<Long> createCartEntries(final ProductRepository productRepository,
                                         final CartEntryRepository cartEntryRepository) {

        final CartEntry cartEntry = new CartEntry(productRepository.findById(1L).get(), 1);
        final CartEntry cartEntry2 = new CartEntry(productRepository.findById(2L).get(), 2);

        cartEntryRepository.save(cartEntry);
        cartEntryRepository.save(cartEntry2);

        final List<Long> ids = new ArrayList<>();
        ids.add(cartEntry.getId());
        ids.add(cartEntry2.getId());

        return ids;
    }

    private void products(final ProductRepository productRepository) {
        log.info("===== CREATING PRODUCTS =====");
        final List<Long> productIds = createProducts(productRepository);
        log.info("---------------------------");
        log.info("");

        log.info("===== GETTING ALL PRODUCTS =====");
        getAllProducts(productRepository);
        log.info("---------------------------");
        log.info("");

        log.info("===== GET PRODUCT BY ID =====");
        getProductById(productRepository, productIds.get(1));
        log.info("---------------------------");
        log.info("");

        log.info("===== GET PRODUCT BY NAME PR001 =====");
        getProductsByName(productRepository, "PR001");
        log.info("---------------------------");
        log.info("");
    }

    private void getProductsByName(final ProductRepository productRepository, final String name) {

        log.info(productRepository.findByName(name).toString());
    }

    private void getProductById(final ProductRepository productRepository, final long id) {

        log.info(productRepository.findById(id).toString());
    }

    private void getAllProducts(final ProductRepository productRepository) {

        final Iterable<Product> products = productRepository.findAll();

        for (Product p : products) {
            log.info(p.toString());
        }
    }

    private List<Long> createProducts(final ProductRepository productRepository) {

        final Product product = new Product("PR001", "DESCRIPTION_001", new BigDecimal("19.90"));
        final Product product2 = new Product("PR002", "DESCRIPTION_002", new BigDecimal("29.90"));
        final Product product3 = new Product("PR003", "DESCRIPTION_003", new BigDecimal("39.90"));
        productRepository.save(product);
        productRepository.save(product2);
        productRepository.save(product3);

        final List<Long> ids = new ArrayList<>();
        ids.add(product.getId());
        ids.add(product2.getId());
        ids.add(product3.getId());

        return ids;
    }
}