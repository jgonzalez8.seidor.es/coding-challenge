package com.coding.challenge.converter;

import com.coding.challenge.dto.CartDTO;
import com.coding.challenge.entity.Cart;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CartConverter {

    @Autowired
    private ModelMapper modelMapper;

    public CartDTO convertToDto(Cart cart) {
        return modelMapper.map(cart, CartDTO.class);
    }

    public Cart convertToEntity(CartDTO cartDTO) {
        return modelMapper.map(cartDTO, Cart.class);
    }
}
