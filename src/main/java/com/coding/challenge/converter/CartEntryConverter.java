package com.coding.challenge.converter;

import com.coding.challenge.dto.CartEntryDTO;
import com.coding.challenge.entity.CartEntry;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CartEntryConverter {

    @Autowired
    private ModelMapper modelMapper;

    public CartEntryDTO convertToDto(CartEntry cartEntry) {
        return modelMapper.map(cartEntry, CartEntryDTO.class);
    }

    public CartEntry convertToEntity(CartEntryDTO cartEntryDTO) {
        return modelMapper.map(cartEntryDTO, CartEntry.class);
    }
}
