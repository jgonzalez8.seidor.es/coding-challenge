package com.coding.challenge.service;

import com.coding.challenge.dto.ProductDTO;

import java.util.Set;

public interface ProductService {

    /**
     * Returns the Product related to the provided idProduct
     *
     * @param idProduct The id of the product.
     * @return The productDTO obtained from DB
     */
    ProductDTO getProductById(long idProduct);

    /**
     * Returns all the products in DB
     *
     * @return all the products in DB
     */
    Set<ProductDTO> getAllProducts();

}
