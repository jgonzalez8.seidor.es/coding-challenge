package com.coding.challenge.service;

import com.coding.challenge.dto.CartDTO;

public interface CartService {

    /**
     * Returns the cartDTO related to the provided cartId
     *
     * @param cartId The id of the cart in DB.
     * @return The cart obtained from DB.
     */
    CartDTO getCartById(long cartId);

    /**
     * Modify the cart with a new product and save it to DB
     *
     * @param cartId    The id of the cart in DB.
     * @param productId The if of the product in DB.
     * @param quantity  The quantity of the products to add or remove to the cart.
     * @return The CartDTO updated.
     */
    CartDTO updateCart(long cartId, long productId, long quantity);
}
