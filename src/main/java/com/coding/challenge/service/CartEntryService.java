package com.coding.challenge.service;

import com.coding.challenge.dto.CartEntryDTO;

public interface CartEntryService {

    /**
     * Create a new CartEntry with the product and the quantity provided.
     *
     * @param productId The id of the product.
     * @param quantity  The quantity of the product.
     * @return The id of the new Cart Entry.
     */
    Long createCartEntry(long productId, long quantity);

    /**
     * Returns the Cart entry related to the cartEntryId provided
     *
     * @param cartEntryId The id of the cartEntry
     * @return The cartDTO find in DB.
     */
    CartEntryDTO getCartEntryById(long cartEntryId);
}
