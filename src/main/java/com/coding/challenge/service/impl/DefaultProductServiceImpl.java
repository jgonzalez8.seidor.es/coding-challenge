package com.coding.challenge.service.impl;

import com.coding.challenge.converter.ProductConverter;
import com.coding.challenge.dto.ProductDTO;
import com.coding.challenge.entity.Product;
import com.coding.challenge.repository.ProductRepository;
import com.coding.challenge.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class DefaultProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductConverter productConverter;


    @Override
    public ProductDTO getProductById(final long idProduct) {

        final Optional<Product> productOptional = productRepository.findById(idProduct);

        return productOptional.map(this::convertToDto).orElse(null);
    }

    @Override
    public Set<ProductDTO> getAllProducts() {

        final Iterable<Product> products = productRepository.findAll();
        final Set<ProductDTO> productDTOSet = new HashSet<>();
        products.forEach(product ->
                productDTOSet.add(convertToDto(product))
        );

        return productDTOSet;
    }

    private ProductDTO convertToDto(final Product product) {
        return productConverter.convertToDto(product);
    }

    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public ProductConverter getProductConverter() {
        return productConverter;
    }
}
