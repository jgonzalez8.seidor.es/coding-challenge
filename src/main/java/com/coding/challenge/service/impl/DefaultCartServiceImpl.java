package com.coding.challenge.service.impl;

import com.coding.challenge.converter.CartConverter;
import com.coding.challenge.converter.CartEntryConverter;
import com.coding.challenge.dto.CartDTO;
import com.coding.challenge.dto.CartEntryDTO;
import com.coding.challenge.entity.Cart;
import com.coding.challenge.entity.CartEntry;
import com.coding.challenge.repository.CartEntryRepository;
import com.coding.challenge.repository.CartRepository;
import com.coding.challenge.service.CartEntryService;
import com.coding.challenge.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Optional;

@Service
public class DefaultCartServiceImpl implements CartService {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartEntryRepository cartEntryRepository;

    @Autowired
    private CartEntryService cartEntryService;

    @Autowired
    private CartConverter cartConverter;

    @Autowired
    private CartEntryConverter cartEntryConverter;

    @Override
    public CartDTO getCartById(final long cartId) {

        Optional<Cart> cartOptional = cartRepository.findById(cartId);

        return cartOptional.map(this::convertToDto).orElse(null);
    }

    private CartDTO convertToDto(final Cart cart) {
        return cartConverter.convertToDto(cart);
    }

    @Override
    public CartDTO updateCart(final long cartId,
                              final long productId,
                              final long quantity) {

        final Optional<Cart> optionalCart = cartRepository.findById(cartId);

        if (optionalCart.isPresent()) {

            final Cart cart = optionalCart.get();
            if (isProductInCart(cart, productId)) {
                updateQuantityOfProduct(productId, quantity, cart);
            } else {
                addProductToCart(productId, quantity, cart);
            }
            return cartConverter.convertToDto(cart);
        }
        return null;
    }

    private void addProductToCart(final long productId,
                                     final long quantity,
                                     final Cart cart) {

        if (quantity > 0) {
            final Long cartEntryId = cartEntryService.createCartEntry(productId, quantity);
            final CartEntryDTO cartEntryDTO = cartEntryService.getCartEntryById(cartEntryId);

            cart.addCartEntry(cartEntryConverter.convertToEntity(cartEntryDTO));
            cartRepository.save(cart);
        }
    }

    private void updateQuantityOfProduct(final long productId,
                                         final long quantity,
                                         final Cart cart) {

        Optional<CartEntry> optionalCartEntry =
                cart.getCartEntries()
                        .stream()
                        .filter(cartEntry -> cartEntry.getProduct().getId().equals(productId)).findFirst();

        if (optionalCartEntry.isPresent()) {
            final CartEntry cartEntry = optionalCartEntry.get();
            if (checkRemoveProduct(quantity, cartEntry)) {
                removeCartEntry(productId, cart, cartEntry);
            } else {
                updateQuantityProduct(quantity, cartEntry);
            }

        }
    }

    private void updateQuantityProduct(final long quantity, final CartEntry cartEntry) {
        cartEntry.setQuantity(cartEntry.getQuantity() + quantity);
        cartEntryRepository.save(cartEntry);
    }

    private void removeCartEntry(final long productId, final Cart cart, final CartEntry cartEntry) {
        cart.getCartEntries().removeIf(ct -> ct.getProduct().getId().equals(productId));
        cartEntryRepository.delete(cartEntry);
    }

    private boolean checkRemoveProduct(final long quantity, final CartEntry cartEntry) {
        return (cartEntry.getQuantity() + quantity) < 1;
    }

    private boolean isProductInCart(final Cart cart, final long productId) {

        Optional<CartEntry> optionalCartEntry =
                cart.getCartEntries()
                        .stream()
                        .filter(cartEntry -> cartEntry.getProduct().getId().equals(productId)).findFirst();

        return optionalCartEntry.isPresent();
    }

    public CartRepository getCartRepository() {
        return cartRepository;
    }

    public CartEntryService getCartEntryService() {
        return cartEntryService;
    }
}
