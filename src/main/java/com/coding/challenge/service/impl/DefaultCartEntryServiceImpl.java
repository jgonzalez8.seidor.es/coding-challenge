package com.coding.challenge.service.impl;

import com.coding.challenge.converter.CartEntryConverter;
import com.coding.challenge.dto.CartEntryDTO;
import com.coding.challenge.entity.CartEntry;
import com.coding.challenge.entity.Product;
import com.coding.challenge.repository.CartEntryRepository;
import com.coding.challenge.repository.ProductRepository;
import com.coding.challenge.service.CartEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DefaultCartEntryServiceImpl implements CartEntryService {

    @Autowired
    private CartEntryRepository cartEntryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CartEntryConverter cartEntryConverter;

    @Override
    public Long createCartEntry(final long productId, final long quantity) {

        final Optional<Product> productOptional = productRepository.findById(productId);

        if (productOptional.isPresent()) {
            final CartEntry cartEntry = new CartEntry(productOptional.get(), quantity);
            cartEntryRepository.save(cartEntry);
            return cartEntry.getId();
        }

        return null;
    }

    @Override
    public CartEntryDTO getCartEntryById(final long cartEntryId) {

        Optional<CartEntry> optionalCartEntry = cartEntryRepository.findById(cartEntryId);
        return optionalCartEntry.map(this::convertToDto).orElse(null);
    }

    private CartEntryDTO convertToDto(final CartEntry cartEntry) {
        return cartEntryConverter.convertToDto(cartEntry);
    }

    public CartEntryRepository getCartEntryRepository() {
        return cartEntryRepository;
    }

    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public CartEntryConverter getCartEntryConverter() {
        return cartEntryConverter;
    }
}
