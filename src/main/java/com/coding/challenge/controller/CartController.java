package com.coding.challenge.controller;

import com.coding.challenge.dto.CartDTO;
import com.coding.challenge.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    /**
     * Returns the CartDTO related to the provided id param.
     *
     * @param id The id of the Cart.
     * @return The CartDTO in json format.
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public CartDTO getCartById(@PathVariable long id) {
        return cartService.getCartById(id);
    }

    /**
     * Update the Cart with a product.
     * It is possible to add a new product indicating the quantity
     * or modify the quantity of a product actually present in the cart.
     * If the quantity is negative, it is possible to reduce the quantity of the product or even remove it.
     *
     * @param cartId    The id of the cart.
     * @param productId The id of the product.
     * @param quantity  The quantity of products to add or subtract to the cart.
     * @return The CartDTO updated with the new product in json format.
     */
    @PutMapping(value = "/updateCart", produces = "application/json", params = {"cartId", "productId", "quantity"})
    public CartDTO updateCart(@RequestParam long cartId,
                                    @RequestParam long productId,
                                    @RequestParam long quantity) {

        return cartService.updateCart(cartId, productId, quantity);
    }
}
