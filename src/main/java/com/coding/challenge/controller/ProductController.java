package com.coding.challenge.controller;

import com.coding.challenge.dto.ProductDTO;
import com.coding.challenge.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/v1/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * Returns the productDTO related to the provided id param.
     *
     * @param id The id of the product.
     * @return The productDTO in json format.
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ProductDTO getProductById(@PathVariable long id) {
        return productService.getProductById(id);
    }

    /**
     * Returns all products in DB.
     * @return All products in json format.
     */
    @GetMapping(value = "/all", produces = "application/json")
    public Set<ProductDTO> getAllProducts() {
        return productService.getAllProducts();
    }

    public ProductService getProductService() {
        return productService;
    }
}
