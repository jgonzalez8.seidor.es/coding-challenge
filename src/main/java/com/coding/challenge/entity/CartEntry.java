package com.coding.challenge.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;


@Entity
public class CartEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @JoinColumn(nullable = false)
    @ManyToOne(optional = false)
    private Product product;

    @Column(nullable = false)
    private long quantity;

    public CartEntry() {

    }

    public CartEntry(final Product product, final long quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }

    public long getQuantity() {
        return quantity;
    }

    public BigDecimal getPrice() {
        return product.getPrice().multiply(new BigDecimal(quantity));
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setProduct(final Product product) {
        this.product = product;
    }

    public void setQuantity(final long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "{\"_class\":\"CartEntry\", " +
                "\"id\":" + (id == null ? "null" : "\"" + id + "\"") + ", " +
                "\"product\":" + (product == null ? "null" : product.getId()) + ", " +
                "\"quantity\":\"" + quantity + "\"" + ", " +
                "\"price\":\"" + getPrice() + "\"" +
                "}";
    }

    @Override
    public boolean equals(final Object o) {

        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final CartEntry cart = (CartEntry) o;
        return id.equals(cart.getId());
    }
}
