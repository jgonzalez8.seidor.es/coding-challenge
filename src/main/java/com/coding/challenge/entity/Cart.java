package com.coding.challenge.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.Set;

@Entity
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @JoinColumn
    @OneToMany(fetch = javax.persistence.FetchType.EAGER, orphanRemoval = true)
    private Set<CartEntry> cartEntries;

    public Cart() {

    }

    public Cart(final Set<CartEntry> cartEntries) {
        this.cartEntries = cartEntries;
    }

    public Long getId() {
        return id;
    }

    public Set<CartEntry> getCartEntries() {
        return cartEntries;
    }

    public boolean addCartEntry(final CartEntry cartEntry) {
        return getCartEntries().add(cartEntry);
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setCartEntries(final Set<CartEntry> cartEntries) {
        this.cartEntries = cartEntries;
    }

    @Override
    public String toString() {
        return "{\"_class\":\"Cart\", " +
                "\"id\":" + (id == null ? "null" : "\"" + id + "\"") + ", " +
                "\"totalPrice\":" + getTotalPrice() +
                "}";
    }

    public BigDecimal getTotalPrice() {

        return getCartEntries().stream().map(CartEntry::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public boolean equals(final Object o) {

        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Cart cart = (Cart) o;
        return id.equals(cart.getId());
    }

}
